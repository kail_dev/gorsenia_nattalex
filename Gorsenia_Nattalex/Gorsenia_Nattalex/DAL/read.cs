﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Nattalex.DAL
{
  public  class read
    {
        public List<product> XMLREADER()
        {
            string xmlText = File.ReadAllText(Properties.Settings.Default.Sciezka_do_XML+Properties.Settings.Default.Nazwa_pliku_XML);
            StringReader stringReader = new StringReader(xmlText);
            XmlSerializer serializer = new XmlSerializer(typeof(List<product>), new XmlRootAttribute("products"));
            //     XmlSerializer ss = new XmlSerializer(typeof(List));
            List<product> productList = (List<product>)serializer.Deserialize(stringReader);

            return productList;
        }


        public rss XMLREADER2()

        {
            rss rss = null;

            XmlSerializer serializer = new XmlSerializer(typeof(rss));
            string path = (Properties.Settings.Default.Sciezka_do_XML + Properties.Settings.Default.Nazwa_pliku_XML);
            StreamReader reader = new StreamReader(path);
            rss = (rss)serializer.Deserialize(reader);
            return rss;

        }

    }
}
