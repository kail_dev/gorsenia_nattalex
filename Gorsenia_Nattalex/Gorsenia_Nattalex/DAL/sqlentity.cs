﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nattalex.DAL
{
    public class sqlentity
    {
        public int TWRXML_ID { get; set; }
        public string TWRXML_KOD { get; set; }
        public string TWRXML_TWR_EAN { get; set; }
        public int TWRXML_ILOSC { get; set; }
    }
}
