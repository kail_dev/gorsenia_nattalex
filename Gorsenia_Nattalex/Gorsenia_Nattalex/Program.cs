﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nattalex.DAL;
using System.Xml.Serialization;
using NLog;

namespace Nattalex
{

    // UWAGA: Wygenerowany kod może wymagać co najmniej programu .NET Framework 4.5 lub .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class rss
    {

        private rssChannel channelField;

        private decimal versionField;

        /// <remarks/>
        public rssChannel channel
        {
            get
            {
                return this.channelField;
            }
            set
            {
                this.channelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rssChannel
    {

        private string linkField;

        private rssChannelItem[] itemField;

        /// <remarks/>
        public string link
        {
            get
            {
                return this.linkField;
            }
            set
            {
                this.linkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("item")]
        public rssChannelItem[] item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class rssChannelItem
    {

        private string linkField;

        private string titleField;

        private string custom_label_0Field;

        private string custom_label_1Field;

        private string descriptionField;

        private string colorField;

        private string sizeField;

        private string item_group_idField;

        private string priceField;

        private string image_linkField;

        private string additional_image_linkField;

        private string gtinField;

        private string mpnField;

        private string categoryField;

        /// <remarks/>
        public string link
        {
            get
            {
                return this.linkField;
            }
            set
            {
                this.linkField = value;
            }
        }

        /// <remarks/>
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string custom_label_0
        {
            get
            {
                return this.custom_label_0Field;
            }
            set
            {
                this.custom_label_0Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string custom_label_1
        {
            get
            {
                return this.custom_label_1Field;
            }
            set
            {
                this.custom_label_1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string color
        {
            get
            {
                return this.colorField;
            }
            set
            {
                this.colorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string size
        {
            get
            {
                return this.sizeField;
            }
            set
            {
                this.sizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string item_group_id
        {
            get
            {
                return this.item_group_idField;
            }
            set
            {
                this.item_group_idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string price
        {
            get
            {
                return this.priceField;
            }
            set
            {
                this.priceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string image_link
        {
            get
            {
                return this.image_linkField;
            }
            set
            {
                this.image_linkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string additional_image_link
        {
            get
            {
                return this.additional_image_linkField;
            }
            set
            {
                this.additional_image_linkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string gtin
        {
            get
            {
                return this.gtinField;
            }
            set
            {
                this.gtinField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string mpn
        {
            get
            {
                return this.mpnField;
            }
            set
            {
                this.mpnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://base.google.com/ns/1.0")]
        public string category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }
    }

    public class product
    {

        public string sku { get; set; }
        public string ean { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public string color { get; set; }
        public string category { get; set; }

        public string size { get; set; }
        public string qty { get; set; }
        public string price { get; set; }
        public string assortment { get; set; }
        public string type { get; set; }
        public string manufacturer { get; set; }


        [XmlArray(ElementName = "media", IsNullable = true)]
        [XmlArrayItem(ElementName = "image", IsNullable = true)]
        public string[] image
        {
            get; set;
        }

    }
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            DateTime thisDay = DateTime.Now;
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = @"log\" + "log" + thisDay + ".txt" };

            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            // Rules for mapping loggers to targets            
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);
            NLog.LogManager.Configuration = config;
            optima opt = new optima();
            opt.LogowanieAutomatyczne();
            opt.Rabaty();




            /*       download xml = new download();

                   var down = xml.PobierzXML();

                   if (down == true)
                   {
                       logger.Info("Pobieram xml z linka {0} . Scieżka do pliku xml: {1}", Properties.Settings.Default.Link_do_XML, Properties.Settings.Default.Sciezka_do_XML + Properties.Settings.Default.Nazwa_pliku_XML);
                       logger.Info("XML został pobrany");

                   }
                   else
                   {
                       logger.Error("Błąd pobierania xmla");

                   }

                   */
            /*    //OBSESIVE =2
                if (Properties.Settings.Default.Format_XML == 2)
                {
                    optima log = new optima();
                    log.LogowanieAutomatyczne();

                    read xmls = new read();
                    var items = xmls.XMLREADER2();

                    foreach (var item in items.channel.item)
                    {
                        check file = new check();

                        if (item.image_link.Length > 1)
                        {
                            converter link1 = new converter();
                            string nazwazdjecia1 = link1.NameFromLink(item.image_link);
                            check img1 = new check();
                            var checkimg = img1.filexist(nazwazdjecia1);
                            if (checkimg == false)
                            {
                                logger.Info("Pobieram zdjecie {0}", nazwazdjecia1);

                                download imgdow = new download();
                                imgdow.PobierzIMG(item.image_link, nazwazdjecia1);
                            }
                            else
                            {
                                logger.Info("Posiadam juz zdjęcie o nazwie {0}", nazwazdjecia1);
                            }









                        }
                        else
                        {
                            logger.Error("Brak linka w xmlu");
                        }

                        if (item.additional_image_link.Length > 1)
                        {
                            converter link1 = new converter();
                            string nazwazdjecia2 = link1.NameFromLink(item.additional_image_link);
                            check img1 = new check();
                            var checkimg = img1.filexist(nazwazdjecia2);
                            if (checkimg == false)
                            {
                                logger.Info("Pobieram zdjecie {0}", nazwazdjecia2);

                                download imgdow = new download();
                                imgdow.PobierzIMG(item.additional_image_link, nazwazdjecia2);
                            }
                            else
                            {
                                logger.Info("Posiadam juz zdjęcie o nazwie {0}", nazwazdjecia2);
                            }

                        }
                        else
                        {
                            logger.Error("brak linku 2 w xml");
                        }
                    }

                    var items2 = xmls.XMLREADER2();
                    int ile = 0;
                    int ileblad = 0;
                    foreach (var item in items2.channel.item)
                    {
                        converter link1 = new converter();
                        string nazwazdjecia1 = link1.NameFromLink(item.image_link);

                        string nazwazdjecia2 = link1.NameFromLink(item.additional_image_link);

                        converter pricedec = new converter();
                        decimal pricedecimal = pricedec.PricetoIntPLN(item.price);




                        logger.Info("Dodaje karte towarowa  \n" +
                            "Kod {0}  \n" +
                            "Ean: {1} \n" +
                            "Nazwa: {2} \n", item.mpn, item.gtin, item.title);
                        var adds = log.AddXML2(item.mpn, item.title, item.gtin, item.color, pricedecimal, item.description, item.size, nazwazdjecia1, nazwazdjecia2, item.category, 1, "ss");
                        if (adds == true)
                        {
                            ile++;
                            logger.Info("Karta dodana prawidłowo");
                        }
                        else
                        {
                            ileblad++;
                            logger.Error("Błąd dodawania karty towarowej");
                        }

                    }
                    logger.Info("Dodalem kart towarowych poprawnie: {0}", ile);
                    logger.Info("Kart nie dodanych {0}", ileblad);





                }





                // XML FORMAT 1 GORSENIA EFEKT
                //GORSNIA 3
                else if (Properties.Settings.Default.Format_XML == 3)
                {
                    int ile = 0;
                    int ilebad = 0;
                    read xmls1 = new read();
                    var items1 = xmls1.XMLREADER();

                    logger.Info("Pozycji w pliku: {0}", items1.Count);
                    foreach (var itemss in items1)
                    {


                        if (itemss.image.Length > 0)
                        {

                            converter con = new converter();
                            var linkname1 = con.NameFromLink(itemss.image[0]);
                            check filevv = new check();
                            var img1 = filevv.filexist(linkname1);
                            if (img1 == false)
                            {
                                download newimg = new download();
                                logger.Info("Pobieram zdjecie do produktu: {0} ", itemss.sku);
                                newimg.PobierzIMG(itemss.image[0], linkname1);

                            }
                            else
                            {
                                logger.Error("Posiadam juz zdj2ecie do produktu: {0} ", itemss.sku);
                            }
                        }

                    }

                    optima opt = new optima();
                    opt.LogowanieAutomatyczne();
                    read xmls2 = new read();
                    var items2 = xmls2.XMLREADER();
                    foreach (var item2 in items2)
                    {
                        converter conv = new converter();
                        decimal price = conv.PricetoIntWithoutPLN(item2.price);
                        int QtyInt = conv.QtyToInt(item2.qty);
                        string grupka = conv.OneGroup(item2.category);
                        try
                        {
                            string nazwazd1 = "";
                            string nazwazd2 = "";
                            if (item2.image.Length == 1)
                            {
                                nazwazd1 = item2.image[0];
                                nazwazd2 = "";
                            }
                            else
                            {
                                nazwazd1 = item2.image[0];
                                nazwazd2 = item2.image[1];

                            }



                            // logger.Info("Dodaje karte towarowa o kodzie {0}", item2.sku);
                            logger.Info("Dodaje karte towarowa  \n" +
                          "Kod {0}  \n" +
                          "Ean: {1} \n" +
                          "Nazwa: {2} \n", item2.sku, item2.ean, item2.name);
                            var adds = opt.Add(item2.sku, item2.name, item2.ean, item2.color, price, item2.description, item2.size, nazwazd1, nazwazd2, grupka, QtyInt, item2.manufacturer);
                            if (adds == true)
                            {
                                ile++;
                                sqlentity sql = new sqlentity();
                                sql.TWRXML_TWR_EAN = item2.ean;
                                sql.TWRXML_KOD = item2.sku;
                                sql.TWRXML_ILOSC = QtyInt;
                                sqlmetods ins = new sqlmetods();

                                ins.Insert(sql);
                                logger.Info("Karta dodana poprawnie!");
                            }
                            else
                            {
                                ilebad++;
                                logger.Error("Blad dodawania karty towarowej");
                                logger.Error("#####################################################################");
                                sqlmetods check = new sqlmetods();
                                var ids = check.CheckTwrID(item2.sku);
                                if (ids.Count == 0)
                                {
                                    sqlentity sql = new sqlentity();
                                    sql.TWRXML_TWR_EAN = item2.ean;
                                    sql.TWRXML_KOD = item2.sku;
                                    sql.TWRXML_ILOSC = QtyInt;
                                    sqlmetods ins = new sqlmetods();

                                    ins.Insert(sql);
                                }


                                foreach (var id in ids)
                                {
                                    sqlmetods up = new sqlmetods();
                                    up.UpdateQty(id.TWRXML_ID, QtyInt);
                                    logger.Info("Aktualizuje ilosc do b2b! Kod towaru {0} , Ilość aktualizowana: {1}", item2.sku, QtyInt);
                                }

                            }
                        }
                        catch (Exception e)
                        {
                            logger.Error(e);
                        }

                    }
                    logger.Info("Dodalem kart towarowych poprawnie: {0}", ile);
                    logger.Info("Karty nie dodane: {0}", ilebad);


                }








                //efeket = 1

                else if (Properties.Settings.Default.Format_XML == 1)
                {

                    int ile = 0;
                    int ilebad = 0;
                    read xmls1 = new read();
                    var items1 = xmls1.XMLREADER();

                    logger.Info("Pozycji w pliku: {0}", items1.Count);
                    foreach (var itemss in items1)
                    {
                        check grups = new check();
                        var doit = grups.checkgroup(itemss.manufacturer);
                        if (doit == true)
                        {
                            if (itemss.image.Length > 0)
                            {

                                converter con = new converter();
                                var linkname1 = con.NameFromLink(itemss.image[0]);
                                check filevv = new check();
                                var img1 = filevv.filexist(linkname1);
                                if (img1 == false)
                                {
                                    download newimg = new download();
                                    logger.Info("Pobieram zdjecie do produktu: {0} ", itemss.sku);
                                    newimg.PobierzIMG(itemss.image[0], linkname1);

                                }
                                else
                                {
                                    logger.Error("Posiadam juz zdj2ecie do produktu: {0} ", itemss.sku);
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("ZLA GRUPA NIE POBIERA");
                        }

                    }

                    optima opt = new optima();
                    opt.LogowanieAutomatyczne();
                    read xmls2 = new read();
                    var items2 = xmls2.XMLREADER();
                    foreach (var item2 in items2)
                    {
                        check grups2 = new check();
                        var doit = grups2.checkgroup(item2.manufacturer);
                        if (doit == true)
                        {


                            converter conv = new converter();
                            decimal price = conv.PricetoIntWithoutPLN(item2.price);
                            int QtyInt = conv.QtyToInt(item2.qty);
                            string grupka = conv.OneGroup(item2.manufacturer);
                            try
                            {
                                string nazwazd1 = "";
                                string nazwazd2 = "";
                                if (item2.image.Length == 1)
                                {
                                    nazwazd1 = item2.image[0];
                                    nazwazd2 = "";
                                }
                                else
                                {
                                    nazwazd1 = item2.image[0];
                                    nazwazd2 = item2.image[1];

                                }



                                // logger.Info("Dodaje karte towarowa o kodzie {0}", item2.sku);
                                logger.Info("Dodaje karte towarowa  \n" +
                              "Kod {0}  \n" +
                              "Ean: {1} \n" +
                              "Nazwa: {2} \n", item2.sku, item2.ean, item2.name);
                                var adds = opt.Add(item2.sku, item2.name, item2.ean, item2.color, price, item2.description, item2.size, nazwazd1, nazwazd2, grupka, QtyInt, item2.manufacturer);
                                if (adds == true)
                                {
                                    ile++;
                                    sqlentity sql = new sqlentity();
                                    sql.TWRXML_TWR_EAN = item2.ean;
                                    sql.TWRXML_KOD = item2.sku;
                                    sql.TWRXML_ILOSC = QtyInt;
                                    sqlmetods ins = new sqlmetods();

                                    ins.Insert(sql);
                                    logger.Info("Karta dodana poprawnie!");
                                }
                                else
                                {
                                    ilebad++;
                                    logger.Error("Blad dodawania karty towarowej");
                                    logger.Error("#####################################################################");
                                    sqlmetods check = new sqlmetods();
                                    var ids = check.CheckTwrID(item2.sku);
                                    if (ids.Count == 0)
                                    {
                                        sqlentity sql = new sqlentity();
                                        sql.TWRXML_TWR_EAN = item2.ean;
                                        sql.TWRXML_KOD = item2.sku;
                                        sql.TWRXML_ILOSC = QtyInt;
                                        sqlmetods ins = new sqlmetods();

                                        ins.Insert(sql);
                                    }


                                    foreach (var id in ids)
                                    {
                                        sqlmetods up = new sqlmetods();
                                        up.UpdateQty(id.TWRXML_ID, QtyInt);
                                        logger.Info("Aktualizuje ilosc do b2b! Kod towaru {0} , Ilość aktualizowana: {1}", item2.sku, QtyInt);
                                    }

                                }
                            }
                            catch (Exception e)
                            {
                                logger.Error(e);
                            }



                        logger.Info("Dodalem kart towarowych poprawnie: {0}", ile);
                        logger.Info("Karty nie dodane: {0}", ilebad);
                        }
                        else
                        {
                            Console.WriteLine("Zla grupa towarowa pomijam");
                        }
                    }
                    }



                else
                {
                    logger.Error("Zly format XML!");
                }






                }
        
             */
        }  
        }
        

        }
    
